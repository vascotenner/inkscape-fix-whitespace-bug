#! /usr/bin/python
'''
Copyright (C) 2005 Aaron Spike, aaron@ekips.org
              2015 Vasco Tenner

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
'''

import sys
sys.path.append('/usr/share/inkscape/extensions')
import inkex
from inkex import errormsg
import gettext
_ = gettext.gettext

class MyEffect(inkex.Effect):

    def affect(self, args=sys.argv[1:], output=True):
        """Affect an SVG document with a callback effect"""
        self.svg_file = args[-1]
        self.getoptions(args)
        self.parse()

    def parse(self, filename=None):
        """Parse document in specified file or on stdin"""

        # First try to open the file from the function argument
        if filename != None:
            try:
                stream = open(filename, 'r')
            except Exception:
                errormsg(_("Unable to open specified file: %s") % filename)
                sys.exit()

        # If it wasn't specified, try to open the file specified as
        # an object member
        elif self.svg_file != None:
            try:
                stream = open(self.svg_file, 'r')
            except Exception:
                errormsg(_("Unable to open object member file: %s") % self.svg_file)
                sys.exit()

        # Finally, if the filename was not specified anywhere, use
        # standard input stream
        else:
            stream = sys.stdin
        
        print(stream.read().replace('white-space:normal;',''))

if __name__ == '__main__':
    e = MyEffect()
    e.affect()


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 encoding=utf-8 textwidth=99
