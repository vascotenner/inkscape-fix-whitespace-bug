There is a bug in chromium, that causes that displaying text-boxes from inkscape with overlapping symbols. This extension fixes that in the svg file.

See also:

* https://bugs.launchpad.net/inkscape/+bug/1468713
* https://code.google.com/p/chromium/issues/detail?id=492596